<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/productoDAO.php";
class Producto{
    private $id_producto;
    private $nombre;
    private $talla;
    private $color;
    private $precio;
    private $cantidad;
    
    public function Producto($id_producto="", $nombre="", $talla="", $color="", $precio="", $cantidad=""){
        $this -> id_producto = $id_producto;
        $this -> nombre = $nombre;
        $this -> talla = $talla;
        $this -> color = $color;
        $this -> precio = $precio;
        $this -> cantidad = $cantidad;    
        $this -> conexion = new Conexion();
        $this -> productoDAO = new productoDAO($id_producto, $nombre, $talla, $color, $precio, $cantidad);
    }
    
    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> crear());
        $this -> conexion -> cerrar();
    }
        
    
//     public function consultarTodos(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> tipoProductoDAO -> consultarTodos());
//         $tiposProducto = array();
//         while(($resultado = $this -> conexion -> extraer()) != null){
//             array_push($tiposProducto, new TipoProducto($resultado[0], $resultado[1]));            
//         }
//         $this -> conexion -> cerrar();
//         return $tiposProducto;
//     }
    
}