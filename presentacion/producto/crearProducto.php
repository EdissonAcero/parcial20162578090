<?php
include "presentacion/menuAdministrador.php";

// ////////////////////// PRESIONAR EL BOTON //////////////////////////

if (isset($_POST["insertar"])) {
    $items1 = ($_POST["nombre"]);
    $items2 = ($_POST["talla"]);
    $items3 = ($_POST["color"]);
    $items4 = ($_POST["precio"]);
    $items5 = ($_POST["cantidad"]);
    
    while (true) {
        
        // // RECUPERAR LOS VALORES DE LOS ARREGLOS ////////
        $item1 = current($items1);
        $item2 = current($items2);
        $item3 = current($items3);
        $item4 = current($items4);
        $item5 = current($items5);
        
        // //// ASIGNARLOS A VARIABLES ///////////////////
        $nombe = (($item1 !== false) ? $item1 : ", &nbsp;");
        $talla = (($item2 !== false) ? $item2 : ", &nbsp;");
        $color = (($item3 !== false) ? $item3 : ", &nbsp;");
        $precio = (($item4 !== false) ? $item4 : ", &nbsp;");
        $cantidad = (($item5 !== false) ? $item5 : ", &nbsp;");
        
        
        $producto = new Producto("", $nombe, $talla, $color, $precio, $cantidad);
        $producto -> crear();      
        
        // Up! Next Value
        $item1 = next( $items1 );
        $item2 = next( $items2 );
        $item3 = next( $items3 );
        $item4 = next( $items4 );
        $item5 = next( $items5 );
        
        // Check terminator
        if($item1 === false && $item2 === false && $item3 === false && $item4 === false) break;
        
    }
    
}


?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
    		<br>
			<div class="card">
				<div class="card-header">CREAR PRODUCTOS</div>
				<div class="card-body">
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>" method="post">
						<table class="table bg-info"  id="tabla">
							<tr class="fila-fija">
								<td>Nombre<input required name="nombre[]" placeholder="nombre" /></td>
								<td>Talla<input required name="talla[]" placeholder="talla" /></td>
								<td>color<input required name="color[]" placeholder="color" /></td>
								<td>Precio<input required name="precio[]" placeholder="precio" /></td>
								<td>Cantidad<input required name="cantidad[]" placeholder="cantidad" /></td>
							</tr>
						</table>
						<div class="btn-der">
							<button type="submit" name="insertar" class="btn btn-primary">Insertar</button>
        					
        					<button id="adicional" name="adicional" type="button" class="btn btn-warning"> Más + </button>
        				</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(function(){
	// Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
	$("#adicional").on('click', function(){
					$("#tabla tbody tr:eq(0)").clone().removeClass('fila-fija').appendTo("#tabla");
				});
});
</script>

